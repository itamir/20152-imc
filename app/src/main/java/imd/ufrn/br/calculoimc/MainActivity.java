package imd.ufrn.br.calculoimc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import imd.ufrn.br.calculoimc.dto.ResultadoIMC;

public class MainActivity extends Activity {

    private EditText alturaText, pesoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alturaText = (EditText) findViewById(R.id.alturaText);
        pesoText = (EditText) findViewById(R.id.pesoText);
    }

    public void calcularIMC(View view) {

        boolean erro = false;

        if(pesoText.getText().toString().trim().equals("")) {
            pesoText.setError("Digite um peso válido.");
            erro = true;
        }

        if(alturaText.getText().toString().trim().equals("")) {
            alturaText.setError("Digite uma altura válida.");
            erro = true;
        }

        if(!erro) {
            double peso = Double.parseDouble(pesoText.getText().toString());
            double altura = Double.parseDouble(alturaText.getText().toString());
            double imc = peso / Math.pow(altura, 2);

            String resultado = "";

            if( imc < 17)
                resultado = "Muito abaixo do peso.";
            else if( imc >= 17 && imc <= 18.49)
                resultado = "Abaixo do peso.";
            else if (imc >= 18.5 && imc < 25)
                resultado = "Peso normal.";
            else if(imc >= 25)
                resultado = "Gordo";

            ResultadoIMC resultadoIMC = new ResultadoIMC(resultado, imc);
            Intent i = new Intent(MainActivity.this,
                        ResultadoActivity.class);
            i.putExtra("resultadoImc", resultadoIMC);
            startActivity(i);

        }
    }
}

