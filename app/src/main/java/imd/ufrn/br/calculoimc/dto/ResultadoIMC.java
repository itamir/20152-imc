package imd.ufrn.br.calculoimc.dto;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Itamir on 17/08/2015.
 */
public class ResultadoIMC implements Serializable {

    private String resultado;

    private double calculo;

    public ResultadoIMC(String resultado, double calculo) {
        this.resultado = resultado;
        this.calculo = calculo;
    }

    public double getCalculo() {
        return calculo;
    }

    public void setCalculo(double calculo) {
        this.calculo = calculo;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResultadoIMC)) return false;

        ResultadoIMC that = (ResultadoIMC) o;

        if (Double.compare(that.getCalculo(), getCalculo()) != 0) return false;
        return !(getResultado() != null ? !getResultado().equals(that.getResultado()) : that.getResultado() != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getResultado() != null ? getResultado().hashCode() : 0;
        temp = Double.doubleToLongBits(getCalculo());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
