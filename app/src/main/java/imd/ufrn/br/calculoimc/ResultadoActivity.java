package imd.ufrn.br.calculoimc;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import imd.ufrn.br.calculoimc.dto.ResultadoIMC;

public class ResultadoActivity extends Activity {

    private TextView imcResText, resultadoText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        imcResText = (TextView) findViewById(R.id.imcResText);
        resultadoText = (TextView) findViewById(R.id.resultadoText);

        ResultadoIMC resultadoIMC =
                (ResultadoIMC)
                        getIntent().getSerializableExtra("resultadoImc");

        imcResText.setText("IMC: " +resultadoIMC.getCalculo());
        resultadoText.setText("Você está: " +resultadoIMC.getResultado());
    }

}
